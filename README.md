# userManagement



## create Local repository and clonne project

```
mkdir repo
cd repo
git remote add origin https://gitlab.com/sanda.h.robinson/user.git
git branch -M main
git push -uf origin main
```
## Build image backend locally
```
cd user-project
docker build -t registry.gitlab.com/sanda.h.robinson/user
```

## Build image frontend locally

```
cd user-project
docker build -t registry.gitlab.com/sanda.h.robinson/user-app
```

## Publish backend spring image to gitlab container registry

```
docker login registry.gitlab.com
docker push registry.gitlab.com/sanda.h.robinson/user
```

## Publish frontend spring image to gitlab container registry

```
docker push registry.gitlab.com/sanda.h.robinson/user-app
```


## Deploy image to container azure

- login to azure
```
az login
```
- login to azure
```
terraform init
terraform plan
terraform apply 
```

- APP  DNS

```
http://miage-sanda.westus2.azurecontainer.io:80
```


## Technical issue

```
Pour ce projet on a seulement réussi à déployer l'application backend avec une base de données sur Azure mais on 
rencontré de problème pour build l'image du front.

L'Application n'est pas fonctionnelle. 

```

## Equipe member

```
ROBINSON Sanda
LOUDA Aghiles
```

## Resultat


![img_1.png](img_1.png)