const jsonServer = require('json-server');
const middleware = jsonServer.defaults();
const server = jsonServer.create();

server.use(middleware)
server.use(jsonServer.bodyParser)

const userData = require('../server/data/users')

server.get('/api/users', async (req, res, next) => {
   const name = req.query.name_like
   const job = req.query.job_like
   const location = req.query.location_like
   const avaible = req.query.avaible

   console.log("name  ",name)
   console.log("job  ",job)
   console.log("location  ",location)
   console.log("avaible  ",avaible)
   const users = await userData.getUsers.data.users //.filter(user =>  user.name == name && user.job == job && user.location == location && user.avaible == avaible )
   let profiles = users;
   console.log(profiles)
   if (name){ profiles = profiles.filter(u => u.name.toLowerCase().includes(name.toLowerCase()))}
   if (job) { profiles = profiles.filter(u => u.job.toLowerCase().includes(job.toLowerCase()))}
   if (location){ profiles = profiles.filter(u => u.location.toLowerCase().includes(location.toLowerCase())) }
   if (avaible){ profiles = profiles.filter(u => u.available === true) }
  res.status(200).send(profiles);
})


server.listen(3000, () => {
  console.log('JSON server listening on port 3000')
})
