import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { BehaviorSubject, Observable, map, take, tap } from "rxjs";
import { Profile } from "../models/profiles.model"
import { ProfileSearch } from "../models/profile-search.model";
//import { ProfileSearch } from "../models/";

@Injectable({
  providedIn: "root"
})
export class ProfilesGateway {


  constructor(private http :HttpClient) {
  }

  private users$ = new BehaviorSubject<Profile[]>([])
  readonly userObs$ = this.users$.asObservable()


 fetchUsers(search :ProfileSearch): Observable<Profile[]> {
   console.log(search.name)
    let queryParams =  new HttpParams()
    if(search.name ) {queryParams = queryParams.append('name_like', search.name)}
    if(search.job)  {queryParams = queryParams.append("job_like", search.job)}
    if(search.location)  {queryParams = queryParams.append("location_like", search.location)}
    if(search.availableOnly) {queryParams = queryParams.append("avaible", true)}

    return  this.http.get<any>('http://localhost:3000/api/users', {params: queryParams}).pipe(
    map(value => value)
  )}

  }

