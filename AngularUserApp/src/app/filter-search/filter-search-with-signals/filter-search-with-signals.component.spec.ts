import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSearchWithSignalsComponent } from './filter-search-with-signals.component';

describe('FilterSearchWithSignalsComponent', () => {
  let component: FilterSearchWithSignalsComponent;
  let fixture: ComponentFixture<FilterSearchWithSignalsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterSearchWithSignalsComponent]
    });
    fixture = TestBed.createComponent(FilterSearchWithSignalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
