import { Component, computed, signal } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest, startWith, tap, debounceTime, switchMap } from 'rxjs';
import { ProfilesGateway } from '../core/geteways/profile.geteway';
import { Profile } from '../core/models/profiles.model';
import { toSignal, toObservable } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-filter-search-with-signals',
  templateUrl: './filter-search-with-signals.component.html',
  styleUrls: ['./filter-search-with-signals.component.scss']
})
export class FilterSearchWithSignalsComponent {

  constructor( private geteway : ProfilesGateway, private fb:FormBuilder){}

  name = signal('');
  job  = signal('');
  location = signal('');
  available = signal(false);
  search = computed(() => ({
    name : this.name(),
    job  : this.job(),
    location: this.location(),
    availableOnly : this.available()
  }));

  profiles = toSignal(
    toObservable(this.search).pipe(
      debounceTime(200),
      switchMap(search => this.geteway.fetchUsers(search))
    )
  )


}
