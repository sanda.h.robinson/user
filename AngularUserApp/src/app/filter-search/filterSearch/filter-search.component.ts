import { Component, computed, signal } from '@angular/core';
import { ProfilesGateway } from '../core/geteways/profile.geteway';
import { FormBuilder } from '@angular/forms';
import {Observable, combineLatest, debounceTime, startWith, switchMap, tap} from 'rxjs';
import {toObservable, toSignal} from '@angular/core/rxjs-interop'
import { Profile } from '../core/models/profiles.model';

@Component({
  selector: 'app-filter-search',
  templateUrl: './filter-search.component.html',
  styleUrls: ['./filter-search.component.scss']
})
export class FilterSearchComponent {


constructor( private geteway : ProfilesGateway, private fb:FormBuilder){}


search =  this.fb.nonNullable.group({
  name: [''],
  job:[''],
  location: [''],
  available:[false]
})


profile$ :Observable<Profile[]> = this.getProfiles()

private getProfiles(): Observable<Profile[]>{
  const search$ = combineLatest([
    this.search.controls.name.valueChanges.pipe(startWith('')),
    this.search.controls.job.valueChanges.pipe(startWith('')),
    this.search.controls.location.valueChanges.pipe(startWith('')),
    this.search.controls.available.valueChanges.pipe(startWith(false))
  ]).pipe(
     tap(data => console.log(data)),
    debounceTime(300)
  )
  return  search$.pipe(
    switchMap( ([name,job,location,availableOnly]) => this.geteway.fetchUsers({
      name,
      job,
      location,
      availableOnly}))
  )
}

}



