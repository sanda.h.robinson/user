import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilterSearchComponent } from './filter-search/filterSearch/filter-search.component';
import { FilterSearchWithSignalsComponent } from './filter-search/filter-search-with-signals/filter-search-with-signals.component';

const routes: Routes = [
  {path:'', redirectTo: '/filter', pathMatch:'full'},
  {path :'filter', component: FilterSearchComponent},
  {path :'filterwithSignals', component : FilterSearchWithSignalsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
