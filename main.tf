provider "azurerm" {
  skip_provider_registration = true # This is only required when the User, Service Principal, or Identity running Terraform lacks the permissions to register Azure Resource Providers.
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "devops" {
  name     = "Miage"
  location = "West US 2"
}

resource "azurerm_container_group" "miage-container-instance-1" {
  name                = "miage-instance"
  location            = azurerm_resource_group.devops.location
  resource_group_name = azurerm_resource_group.devops.name
  ip_address_type     = "Public"
  dns_name_label      = "miage-sanda"
  os_type             = "Linux"

  container {
    name   = "spring-app"
    image  = "registry.gitlab.com/sanda.h.robinson/user:latest"
    cpu    = "0.5"
    memory = "1.5"

    ports {
      port     = 443
      protocol = "TCP"
    }
  }

  #create container for the DB mysql
   container {
      name   = "mysql-db"
      image  = "mysql:8.0"
      cpu    = "0.5"
      memory = "1.5"

      ports {
        port     = 3306
        protocol = "TCP"
      }

      environment_variables = {
        MYSQL_ROOT_PASSWORD = "rootpassword"
        MYSQL_DATABASE      = "db"
        MYSQL_USER          = "root"
      }
    }


  #create container for the front app
  container {
      name   = "front-app"
      image  = "aghiles98/user-app:latest"
      cpu    = "0.5"
      memory = "1.5"

      ports {
        port     = 80
        protocol = "TCP"
      }
    }


  tags = {
    environment = "testing"
  }
}