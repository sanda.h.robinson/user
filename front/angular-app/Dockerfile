# Stage 1: Build the Angular application
FROM node:20.11.0 AS build

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install --only=dev

# Copy the rest of the application code
COPY . .

# Build the Angular app in production mode
RUN npm run build:prod
# Stage 2: Serve the Angular application using Nginx
FROM node:20.11.0 AS prod

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=prod

COPY . .

COPY --from=build /usr/src/app/dist ./dist

CMD ["npm", "run", "start"]
