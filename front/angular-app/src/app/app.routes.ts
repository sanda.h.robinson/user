import { Routes } from '@angular/router';
import {UserComponent} from "./component/user/user.component";
import {UserFormComponent} from "./component/user-form/user-form.component";

export const routes: Routes = [
  { path: 'users', component: UserComponent },
  { path: 'add-user', component: UserFormComponent },
  { path: '', redirectTo: '/users', pathMatch: 'full' }
];
