import {Component, Input} from '@angular/core';
import {User, UserServiceService} from "../../service/user-service.service";
import {FormsModule, } from "@angular/forms";


@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrl: './user-form.component.css',
  standalone: true,
  imports: [FormsModule],

})
export class UserFormComponent {

  @Input() user: User = { name: '', email: '' };

  constructor(private userService: UserServiceService) { }

  onSubmit() {

    this.userService.createUser(this.user).subscribe(newUser => {
      console.log('User created:', newUser);
    });
  }
}
