import {Component, OnInit} from '@angular/core';
import {User, UserServiceService} from "../../service/user-service.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.css',

})
export class UserComponent implements OnInit{
  users: User[] = [];

  constructor(private userService: UserServiceService) { }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(data => {
      this.users = data;
    });
  }
}
