import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserComponent} from "./component/user/user.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {UserServiceService} from "./service/user-service.service";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/compiler";



@NgModule({
  declarations: [
    UserComponent,

  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  providers:[
    UserServiceService
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
